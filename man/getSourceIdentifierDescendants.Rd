% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/source.R
\name{getSourceIdentifierDescendants}
\alias{getSourceIdentifierDescendants}
\title{Retrieves the descendants of a source concept, descriptor or code.}
\usage{
getSourceIdentifierDescendants(sabb, identifier, pageNumber = 1, pageSize = 25)
}
\arguments{
\item{sabb}{the source abbreviation}

\item{identifier}{the identifier of the concept, descriptor or code in the source vocabulary}

\item{pageNumber}{passed through to the \code{\link{umlsApiGet}} function}

\item{pageSize}{passed through to the \code{\link{umlsApiGet}} function}
}
\value{
list of SourceAtomCluster object
}
\description{
Retrieves the descendants of a source concept, descriptor or code.
}
