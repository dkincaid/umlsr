# umlsr - R package for interfacing with the UMLS REST API. 
The National Library of Medicine maintains a very large metathesaurus of medical terminologies called 
the Unified Medical Language System (UMLS). "The UMLS integrates and distributes key terminology, classification and coding standards, and associated resources to promote creation of more effective and interoperable biomedical information systems and services, including electronic health records."

The UMLS website is at https://www.nlm.nih.gov/research/umls/

## Installation
The package currently can only be installed from this repository (it is not on CRAN yet).

```R
# install.packages('devtools')
devtools::install_git("https://gitlab.com/dkincaid/umlsr.git", build_vignettes=TRUE)
```

## Pre-requisites
In order to use the UMLS Metathesaurus you must obtain a license. If you already have a UMLS license, 
get your API key from your profile and set a UMLS_APIKEY environment variable to that value (e.g. `Sys.setenv(UMLS_APIKEY="abcde12345")`).

If you don't have a UMLS license already, go onto the UMLS website https://www.nlm.nih.gov/databases/umls.html#license_request and follow the directions there to obtain a license.
Once you have your license login, go into your profile and generate an API key. You will need this to use any of the functions in this package.

## Usage
Be sure to set an environment variable UMLS_APIKEY to your UMLS API key.

For details on the API itself see the UMLS REST API documentation at https://documentation.uts.nlm.nih.gov/rest/home.html

The package utilizes camel case function names and most start with "get". We have tried to name the functions so
that they make sense and are easy to find.

### Examples
#### Retrieve a SNOMED-CT code for a UMLS concept CUI
```R
searchUmls("C3670758", returnIdType = "code", sabs = c("SNOMEDCT_US", "SNOMEDCT_VET"))
```

#### Retrieve a UMLS concept CUI from a SNOMED-CT code
```R
searchCode("310381000009107", sabs = c("SNOMEDCT_US", "SNOMEDCT_VET"))
```

