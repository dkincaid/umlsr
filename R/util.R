#' Function for sending requests to the UMLS REST API
#'
#' @param endpoint the endpoint to call
#'
#' @param sabs A character vector of source vocabularies to include in your search.
#' Any root source abbreviation in the UMLS. See the “Abbreviation” column for a list of
#' \href{https://www.nlm.nih.gov/research/umls/sourcereleasedocs/current/source-abbreviations.html}{UMLS source vocabulary abbreviations.}. NULL uses the default of all UMLS source vocabularies. Only used for calls to
#' ATOM endpoints (except for ancestors, descendants, parents and children)
#'
#' @param ttys A character vector of UMLS \href{https://www.nlm.nih.gov/research/umls/knowledge_sources/metathesaurus/release/abbreviations.html#TTYC}{term types}. Only used for calls to
#' ATOM endpoints (except for ancestors, descendants, parents and children)
#'
#' @param includeObsolete true or false. Include content that is obsolete according to the content provider or
#' NLM. Default value is false. Only used for calls to
#' ATOM endpoints (except for ancestors, descendants, parents and children)
#'
#' @param includeSuppressible true or false. Include content that is suppressible according to NLM Editors.
#' Default value is false. Only used for calls to
#' ATOM endpoints (except for ancestors, descendants, parents and children)
#'
#' @param pageNumber the page number to retrieve. Only used for calls to
#' ATOM endpoints (except for ancestors, descendants, parents and children)
#' @param pageSize the page size to use. Only used for calls to
#' ATOM endpoints (except for ancestors, descendants, parents and children)
#' @param language the language to return. Set to null for all languages. Only used for calls to
#' ATOM endpoints (except for ancestors, descendants, parents and children)
#'
#' @param targetSource used for the crosswalk API only Returns codes from the specified UMLS vocabulary
#'
#' @param api_key UMLS API key for authentication
#'
#' @return the content of the API call. The response from the \code{\link[httr]{GET}} is passed through the
#' \code{\link[httr]{content}} function
#'
#' @seealso \url{https://documentation.uts.nlm.nih.gov/rest/home.html}
#'
#' @export
umlsApiGet <- function(endpoint, sabs = NULL, ttys = NULL, includeObsolete = NULL,
                       includeSuppressible = NULL, pageSize = 25, pageNumber = 1, language = "ENG",
                       targetSource = NULL, api_key = Sys.getenv("UMLS_APIKEY")) {

   if (api_key == "") {
      stop("UMLS_APIKEY environment variable is not set properly!")
   }

   url <- glue::glue("https://uts-ws.nlm.nih.gov/rest{endpoint}")

   response <- httr::GET(url,
                         query = list(apiKey = api_key,
                                      sabs = paste0(sabs, collapse = ","),
                                      ttys = paste0(ttys, collapse = ","),
                                      includeObsolete = includeObsolete,
                                      includeSuppressible = includeSuppressible,
                                      language = language,
                                      pageNumber = pageNumber,
                                      pageSize = pageSize,
                                      targetSource = targetSource
                                      ))

   if (response$status_code == 200) {
      return(httr::content(response))
   } else {
      return(response)
   }
}


#' Function for sending requests to the UMLS REST API using an absolute URL.
#'
#' @param url the url to call
#'
#' @param api_key UMLS API key for authentication
#'
#' @return the content of the API call. The response from the \code{\link[httr]{GET}} is passed through the
#' \code{\link[httr]{content}} function
#'
#' @export
umlsApiGetUrl <- function(url, api_key = Sys.getenv("UMLS_APIKEY")) {
   if (api_key == "") {
      stop("UMLS_APIKEY environment variable is not set properly!")
   }

   response <- httr::GET(url, query = list(apiKey = api_key))

   if (response$status_code == 200) {
      return(httr::content(response))
   } else {
      return(response)
   }
}
